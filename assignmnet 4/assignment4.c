#include <stdio.h>

double performanceCost = 500;
double costPerTicket = 3;

int attendencePerPrice(double price){
    double difference = price - 15;
    return 120 -  (difference/5)*20;
}

double totalProfit(int att,double ticketPrice){
    double profitFromTicket = (ticketPrice -costPerTicket)*att;
    return profitFromTicket - 500;
}

int main(){
    double ticketPrice = 5;
    int attendence = attendencePerPrice(ticketPrice);
    double profit = totalProfit(attendence,ticketPrice);
    double lastProfit = profit -1;
    int lastAttendence = 0;
    double lastTicketPrice = 0;

    do{
        lastTicketPrice = ticketPrice;
        ticketPrice +=5;
        lastProfit = profit;
        lastAttendence = attendence;
        attendence = attendencePerPrice(ticketPrice);
        profit = totalProfit(attendencePerPrice(ticketPrice),ticketPrice);
        printf("ticket price : %.2f attendence : %d profit : %.2f \n",ticketPrice,attendence,profit);
    }while(lastProfit < profit);
    printf("highest profit which is : %.2f Rs is achieved when ticket price is %.2f and attendance is : %d",lastProfit,lastTicketPrice,lastAttendence);
    return 0;
}
